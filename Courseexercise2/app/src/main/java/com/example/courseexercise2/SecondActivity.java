package com.example.courseexercise2;

import android.os.Bundle;
import android.widget.TextView;

import androidx.appcompat.app.AppCompatActivity;

public class SecondActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_second);

        if (getIntent().hasExtra("valitettava_arvo")) {
            String text = getIntent().getExtras().getString("valitettava_arvo");
            TextView tv = (TextView) findViewById(R.id.textView);
            tv.setText(text);
        }


    }
}