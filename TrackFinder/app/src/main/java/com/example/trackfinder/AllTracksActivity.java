package com.example.trackfinder;

import androidx.appcompat.app.AppCompatActivity;

import android.os.Bundle;
import android.widget.TextView;

public class AllTracksActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_all_tracks);
        TextView tV_lista = (TextView) findViewById(R.id.ratalista);
        String lista = Track.tracks[0].getAllTracks();
        tV_lista.setText(lista);
    }
}