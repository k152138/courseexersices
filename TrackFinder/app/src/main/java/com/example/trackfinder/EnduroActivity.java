package com.example.trackfinder;

import androidx.appcompat.app.AppCompatActivity;

import android.os.Bundle;
import android.widget.TextView;

import java.util.Calendar;

public class EnduroActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_enduro);

        TextView tV_lista = (TextView) findViewById(R.id.ratalista);
        //Track radat = new Track();
        String lista = Track.tracks[0].getEnduroTracks();
        tV_lista.setText(lista);

    }
}