package com.example.trackfinder;

import androidx.appcompat.app.AppCompatActivity;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ListView;
import android.widget.Toast;

public class LajitActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_lajit);

        AdapterView.OnItemClickListener itemClickListener = new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                if (position==0) {
                    Intent intent = new Intent(LajitActivity.this, EnduroActivity.class);
                    startActivity(intent);
                }
                if (position==1) {
                    Intent intent = new Intent(LajitActivity.this, MotocrossActivity.class);
                    startActivity(intent);
                }




            }
        };
        ListView listView = (ListView) findViewById(R.id.laji_lista);
        listView.setOnItemClickListener(itemClickListener);



    }
}