package com.example.trackfinder;

import androidx.appcompat.app.AppCompatActivity;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Toast;

public class MainActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
    }
    public void haeRataaKlikattu(View view) {


        Intent intent = new Intent(MainActivity.this, LajitActivity.class);
        startActivity(intent);
    }

    public void haeKaikkiKlikattu(View view) {


        Intent intent = new Intent(MainActivity.this, AllTracksActivity.class);
        startActivity(intent);
    }


}