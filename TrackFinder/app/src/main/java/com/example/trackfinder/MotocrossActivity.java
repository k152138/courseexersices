package com.example.trackfinder;

import androidx.appcompat.app.AppCompatActivity;

import android.os.Bundle;
import android.widget.TextView;

public class MotocrossActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_motocross);

        TextView tV_lista = (TextView) findViewById(R.id.ratalista);
        String lista = Track.tracks[0].getMotocrossTracks();
        tV_lista.setText(lista);

    }
}