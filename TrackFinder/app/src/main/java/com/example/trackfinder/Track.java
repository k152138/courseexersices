package com.example.trackfinder;

import java.util.Calendar;

public class Track {
    private String name;
    private String laji;
    private String osoite;
    private int opensAt;
    private int closesAt;

    // Luodaan testausta varten esimerkkidataa, muutaman radan taulukko
    public static final Track[] tracks = {
            new Track("Tehotankki", "enduro","Asemantie 64 Kangasala.", 10, 20),
            new Track("Nokia Mc", "motocross","Testiradantie 4 Nokia.", 13, 21),
            new Track("Paulaniemi", "enduro","Vanha Kuruntie 114 Kuru.", 12, 18),
            new Track("Kaanaa", "motocross", "Moottorikeskuksentie 105 Velaatta.", 8,16)

    };


    //Constructor
    private Track (String name, String laji, String osoite, int opens, int closes) {
        this.name = name;
        this.laji = laji;
        this.osoite = osoite;
        this.opensAt = opens;
        this.closesAt = closes;
    }


    public String getEnduroTracks () {
        int maara = tracks.length;
        //Selvitetään kellonaika tunnin tarkkuudella
        Calendar rightNow = Calendar.getInstance();
        int currentHour = rightNow.get(Calendar.HOUR_OF_DAY);

        StringBuilder kuvaus= new StringBuilder("\n");
        for (Track track : tracks) {
            if (track.laji.equals("enduro")) {
                kuvaus.append(track.name);
                kuvaus.append(", ");
                kuvaus.append(track.osoite);
                kuvaus.append("\n");
                if (currentHour < track.closesAt && currentHour >= track.opensAt) {
                    kuvaus.append("Open now! Closes at ");
                    int sulki = track.closesAt;
                    kuvaus.append(sulki);
                    kuvaus.append(".");
                } else {
                    kuvaus.append("Closed now. Opens at ");
                    int auki = track.opensAt;
                    kuvaus.append(auki);
                    kuvaus.append(".");
                }

                kuvaus.append("\n\n");
            }
        }
        return kuvaus.toString();
    }

    public String getMotocrossTracks () {
        int maara = tracks.length;
        //Selvitetään kellonaika tunnin tarkkuudella
        Calendar rightNow = Calendar.getInstance();
        int currentHour = rightNow.get(Calendar.HOUR_OF_DAY);

        StringBuilder kuvaus= new StringBuilder("\n");
        for (Track track : tracks) {
            if (track.laji.equals("motocross")) {
                kuvaus.append(track.name);
                kuvaus.append(", ");
                kuvaus.append(track.osoite);
                kuvaus.append("\n");
                if (currentHour < track.closesAt && currentHour >= track.opensAt) {
                    kuvaus.append("Open now! Closes at ");
                    int sulki = track.closesAt;
                    kuvaus.append(sulki);
                    kuvaus.append(".");
                } else {
                    kuvaus.append("Closed now. Opens at ");
                    int auki = track.opensAt;
                    kuvaus.append(auki);
                    kuvaus.append(".");
                }

                kuvaus.append("\n\n");
            }
        }
        return kuvaus.toString();
    }

    public String getAllTracks () {
        int maara = tracks.length;
        //Selvitetään kellonaika tunnin tarkkuudella
        Calendar rightNow = Calendar.getInstance();
        int currentHour = rightNow.get(Calendar.HOUR_OF_DAY);

        StringBuilder kuvaus= new StringBuilder("\n");
        for (Track track : tracks) {

                kuvaus.append(track.name);
                kuvaus.append(", ");
                kuvaus.append(track.osoite);
                kuvaus.append("\n");
                kuvaus.append("Sport: ");
                kuvaus.append(track.laji);
                kuvaus.append("\n");
                if (currentHour < track.closesAt && currentHour >= track.opensAt) {
                    kuvaus.append("Open now! Closes at ");
                    int sulki = track.closesAt;
                    kuvaus.append(sulki);
                    kuvaus.append(".");
                } else {
                    kuvaus.append("Closed now. Opens at ");
                    int auki = track.opensAt;
                    kuvaus.append(auki);
                    kuvaus.append(".");
                }

                kuvaus.append("\n\n");
                    }
        return kuvaus.toString();
    }
}
